# Prorocentrum Example

This example shows you how to both download and upload images to the spc.ucsd.edu using python.

## Download

    $ ./spcserver.py --search-param-file=examples/prorocentrum/time_period.txt --image-output-path=examples/prorocentrum/images --meta-output-path=examples/prorocentrum/meta_data.csv -d=True

This downloads images for the 3 following time intervals to `examples/prorocentrum/images`

    2017-3-20 01:00:00, 2017-3-20 15:30:59, 0.03, 0.07, SPCP2
    2017-3-27 13:00:00, 2017-3-27 13:30:00, 0.03, 0.07, SPCP2
    2017-4-10 13:00:00, 2017-4-10 13:40:00, 0.03, 0.07, SPCP2

It will also output the meta data csv file to `examples/prorocentrum/meta_data.csv`

## Upload
Once you have images ready to be uploaded, you can create an `SPCServer` and use its method
`upload` to upload your desired images with their labels.

```
# Create SPCServer
spc = SPCServer()

# Initialize inputs
account_info = {'username': 'kevin', 'password': 'ceratium'}
login_url = 'http://spc.ucsd.edu/data/admin/?next=/data/admin'
predictions = 'examples/prorocentrum/predictions.txt'
label_file = 'examples/prorocentrum/labels.txt'

# Initialize submission dictionary parameters
spc.submit_dict['name'] = 'kevin_test'  # NAME OF LABELING INSTANCE
spc.submit_dict['tag'] = 'kevin_test'
spc.submit_dict['is_machine'] = True  # BOOL FOR MACHINE UPLOAD. IF FALSE, USES USER LOG-IN AS ANNOTATOR NAME
spc.submit_dict['machine_name'] = 'kevin_test'

# Upload images
spc.upload(login_url=login_url,
           account_info=account_info,
           textfile=predictions,
           label_file=label_file)
```
