var baseURL = {
    local:'http://planktivore.ucsd.edu',
    type:'caymans_data',
    rois:{
        'images':'images',
        'imagearchive':'imagearchive',
        'logout_user':'logout_user',
        'get_user':'get_user',
        'login_user':'login_user',
        'labels':'labels',
        'annotators':'annotators',
        'tags':'tags',
        'labeled_images':'label_images',
        'find_image':'find_image'
    }
};

var roiImagesBase = "caymans_data/rois/images/";