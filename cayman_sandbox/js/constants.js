var baseURL = {
    local:'http://planktivore.ucsd.edu',
    type:'caymans_data',
    rois:{
        'images':'images',
        'imagearchive':'imagearchive',
        'logout_user':'logout_user',
        'get_user':'get_user',
        'login_user':'login_user',
        'labels':'labels',
        'annotators':'annotators',
        'tags':'tags',
        'labeled_images':'label_images',
        'find_image':'find_image'
    }
};

var roiLogOutUser = '/caymans_data/rois/logout_user/';
var roiLogInUser = '/caymans_data/rois/login_user/';
var roiGetUser = '/caymans_data/rois/get_user/';
var roiImagesBase = "caymans_data/rois/images/";
var roiArchiveBase = "caymans_data/rois/imagearchive/";
var dailyStatsBase = "caymans_data/rois/roistats/dailystats/";
var roiLabelsBase = '/caymans_data/rois/labels/';
var roiAnnotatorsBase = '/caymans_data/rois/annotators/';
var roiTagsBase = '/caymans_data/rois/tags/';
var roiLabeledImagesBase = '/caymans_data/rois/label_images';
