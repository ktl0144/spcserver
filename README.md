# spcserver

SPCServer - Downloads and upload data to spc.ucsd.edu

## Usage

Run `spcserver.py` to download images from the spc.ucsd.edu. 
Create an `SPCServer` to upload images to the spc.ucsd.edu


### Download
The input requires one file:
1. A search parameter text file containing the start and end dates, minimum/maximum organism length, and camera choice

The output consists of two files:

1. A meta data file (in csv file formating) containing the image's id, min/max length, timestamp, etc.
2. A collection of images pulled from the website based off the desired search parameters.

### Upload
The input requires two files:

1. A predictions text file containing the image file names and their respective enumerated labels
2. A labels text file containing the enumerated labels mapped to their class names


## Examples
See the examples folder for more details